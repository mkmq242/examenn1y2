/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen;

/**
 *
 * @author PC
 */
public class Recibo {
    private String numRecibo;
    private String nombre;
    private int puesto;
    private float nivel;
    private int dias;

    public Recibo(){
     this.numRecibo="";
    this. nombre="";
   this.puesto=0;
   this.nivel=0;
    this. dias=0;
    
}

    public Recibo(String numRecibo, String nombre, int puesto, float nivel, int dias) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.puesto = puesto;
        this.nivel = nivel;
        this.dias = dias;
    }

    public String getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(String numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public float getNivel() {
        return nivel;
    }

    public void setNivel(float nivel) {
        this.nivel = nivel;
    }

    public int getDias() {
        return dias;
    }

    public void setDias(int dias) {
        this.dias = dias;
    }
    
    public float calcularP(){
        
        float pago=0.0f;
        pago=puesto*dias;
        return pago;
        
    }
    
    public float impuesto(){
        float impuesto=0.0f;
        impuesto=nivel*this.calcularP();
        return impuesto;
        
    }
    
    public float total(){
        
        float total=0.0f;
        total=this.calcularP()-this.impuesto();
        return total;
        
    }
}




